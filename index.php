<?php

require 'MiRoutes/UrlEngine.php';

$host = $_SERVER['HTTP_HOST'];

$routes = new MiRoutes\UrlEngine($host);

$routes->get('/ask/{id:num}/', 'displayPage');
$routes->get('/pop/{n:num}/');