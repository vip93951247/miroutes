<?php

namespace MiRoutes;

require 'MatchRoute.php';

class UrlEngine
{
    protected $host;

    public function __construct($host)
    {
        $this->host = $host;
    }

    private $_methods = array(
        'GET'    => array(),
        'POST'   => array(),
        'PUT'    => array(),
        'DELETE' => array(),
        'PATCH'  => array(),
        'HEAD'   => array()
    );

    private $_routes = array(
        'GET'    => array(),
        'POST'   => array(),
        'PUT'    => array(),
        'DELETE' => array(),
        'PATCH'  => array(),
        'HEAD'   => array(),
    );

    private $_patterns = array(
        'num' => '[0-9]+',
        'str' => '[a-zA-Z\.\-_%]+',
        'any' => '[a-zA-Z0-9\.\-_%]+',
    );

    public function addPattern($name, $pattern)
    {
        $this->_patterns[$name] = $pattern;
    }

    public function get($uri, $controller = 'default')
    {
        $action = strtoupper(__FUNCTION__);

        $converted = $this->_doConvert($uri);

        $this->_routes[$action][$converted] = $controller;

        if (!empty($this->_routes[$action])) {
            foreach ($this->_routes[$action] as $url => $method) {
                $this->match($url, $action, $method);
            }
        }
        return true;
    }

    private function _doConvert($route)
    {
        if (false === strpos($route, '{')) {
            return $route;
        }

        return preg_replace_callback(
            '#\{(\w+):(\w+)\}#', array($this, '_doReplace'), $route
        );
    }

    private function _doReplace($match)
    {
        $name = $match[1];
        $pattern = $match[2];

        return $name.'(?'.strtr($pattern, $this->_patterns).')';
    }

    public function match($url, $action, $method)
    {
        $routes = $this->_getRoutes($action);

        if ($this->_doValidateUrl($url, $routes)) {
            return new MatchRoute($routes[$url]);
        }

        return false;
    }

    private function _doValidateUrl($url, $routes)
    {
        return preg_match('ask\/id([0-9]+)', $_SERVER['REQUEST_URI']);
    }

    private function _getRoutes($action)
    {
        return isset($this->_routes[$action]) ? $this->_routes[$action] : array();
    }
}